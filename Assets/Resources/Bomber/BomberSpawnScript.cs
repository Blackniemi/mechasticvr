﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberSpawnScript : MonoBehaviour
{
    GameObject gBomber;

    public float bomberSpawnInterval;
    public float bomberSpawnVariation;
    public float bomberSpawnTimer;

    public bool enabled;

    // Start is called before the first frame update
    void Start()
    {
        gBomber = Resources.Load<GameObject>("Bomber/Bomber");
        enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (enabled)
        {
            bomberSpawnTimer -= Time.deltaTime;

            if (bomberSpawnTimer <= 0f)
            {
                Instantiate(gBomber, gameObject.transform.position, Quaternion.identity);
                bomberSpawnTimer = bomberSpawnInterval + Random.Range(-bomberSpawnVariation, bomberSpawnVariation);
            }
        }
    }
}
