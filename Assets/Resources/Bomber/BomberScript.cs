﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberScript : MonoBehaviour
{
    public GameObject gTarget;
    public Vector3 targetPos;
    public float targetHeightOffset;
    public Vector3 targetAbovePos;
    public float bomberSpeed;
    public float bombingDistance;

    public bool bombed;

    public GameObject myBomb;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("target");
        gTarget = targets[Random.Range(0, targets.Length - 1)];
        targetPos = gTarget.GetComponent<Transform>().position;
        targetAbovePos = targetPos + Vector3.up * targetHeightOffset;
        gameObject.transform.LookAt(targetAbovePos);
        bombed = false;

        myBomb = gameObject.transform.Find("Bomb").gameObject;
        myBomb.GetComponent<Rigidbody>().useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!bombed)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetAbovePos, bomberSpeed * Time.deltaTime);

            float targetDist = Vector3.Distance(gameObject.transform.position, targetAbovePos);
            if (targetDist <= bombingDistance)
            {
                BombsAway();
            }
        }
    }

    void BombsAway()
    {
        myBomb.transform.SetParent(null);
        myBomb.GetComponent<Rigidbody>().useGravity = true;
    }
}
