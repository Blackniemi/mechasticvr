﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirDropAnimationController : MonoBehaviour
{
    /*Trigger OpenAirDrop animation by calling the OpenAirDrop function*/

    private Animator anim;
    Vector3 dropDestination;

    ParticleSystem ps;
    ParticleSystem.EmissionModule pe;

    public bool landed;
    public float dropSpeed;
    public float dropSpinRate;

    public enum Turrets
    {
        RAILGUN
    };

    public Turrets turret;

    GameObject gRailGun;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        ps = gameObject.GetComponent<ParticleSystem>();
        pe = ps.emission;
        pe.enabled = false;
        landed = false;

        gRailGun = Resources.Load<GameObject>("RailGunTurret/RailGun");
    }

    private void Update()
    {

        if (!landed)
        {
            if (Vector3.Distance(gameObject.transform.position, dropDestination) <= 0.3f)
            {
                OpenAirDrop();
                landed = true;
                pe.enabled = true;
                ps.Emit(100);

                switch (turret)
                {
                    case Turrets.RAILGUN:
                        Instantiate(gRailGun, gameObject.transform.position, Quaternion.identity);
                        break;
                }

                Debug.Log("LANDED");
            }
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, dropDestination, dropSpeed * Time.deltaTime);

            gameObject.transform.Rotate(Vector3.up, dropSpinRate * Time.deltaTime, Space.Self);
        }
    }

    public void OpenAirDrop ()
    {
        anim.Play("OpenAirDrop");
    }

    public void ConfigDrop(Vector3 dropPoint, Turrets _turret)
    {
        dropDestination = dropPoint;
        turret = _turret;
    }
}
