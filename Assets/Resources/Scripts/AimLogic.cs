﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimLogic : MonoBehaviour
{
    SphereCollider rangePerimeter;
    Transform tTurretPivot;
    Transform pAmmoSpawnL;
    Transform pAmmoSpawnR;

    GameObject pAmmo;

    //TODO: Consider triggering animations in separate script
    Animator tAnimator;
    bool animPlay;
    bool curPlayState;

    public float range;
    public float turnRateH;
    public float turnRateV;
    public float turnRate;
    public float shootingAngleThreshold;
    public float shootInterval;
    public float shootTimer;

    bool shootRight;

    public Transform tTarget;

    List<GameObject> closeTargets;

    // Start is called before the first frame update
    void Start()
    {
        rangePerimeter = gameObject.GetComponent<SphereCollider>();
        tTurretPivot = gameObject.transform.Find("RailGunPivot");
        pAmmoSpawnL = tTurretPivot.Find("RailGunHead/AmmoSpawnL");
        pAmmoSpawnR = tTurretPivot.Find("RailGunHead/AmmoSpawnR");
        pAmmo = Resources.Load<GameObject>("RailGunTurret/RailGunAmmo");
        closeTargets = new List<GameObject>();

        //TODO: Consider triggering animations in separate script
        tAnimator = this.GetComponent<Animator>();
        animPlay = false;
        curPlayState = false;

        rangePerimeter.radius = range;
        shootRight = true;

        shootTimer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (tTarget != null)
        {
        
            Vector3 targetLookDir = (tTarget.position - tTurretPivot.position).normalized;

            //tTurretPivot.rotation = Quaternion.Euler( Vector3.RotateTowards(tTurretPivot.rotation.eulerAngles, targetLookDir, turnRateRads * Time.deltaTime, 1f));

            Quaternion toRot = Quaternion.LookRotation(targetLookDir, gameObject.transform.up);
            tTurretPivot.rotation = Quaternion.Lerp(tTurretPivot.rotation, toRot, turnRate * Time.deltaTime);

            float angleDiff = Vector3.Angle(tTurretPivot.forward, targetLookDir);
            Debug.Log(angleDiff);
            if (angleDiff < shootingAngleThreshold)
            {
                //TODO: Consider triggering animations in separate script
                animPlay = true;

                shootTimer -= Time.deltaTime;
                if (shootTimer <= 0f)
                {
                    shootTimer = shootInterval;
                    shootRight = !shootRight;
                    if (shootRight)
                        Instantiate(pAmmo, pAmmoSpawnR.position, Quaternion.LookRotation(targetLookDir));
                    else
                        Instantiate(pAmmo, pAmmoSpawnL.position, Quaternion.LookRotation(targetLookDir));
                }
            }

            //TODO: Consider triggering animations in separate script
            else
            {
                
                animPlay = false;
            }
        }

        //TODO: Consider triggering animations in separate script
        if (curPlayState != animPlay)
        {
            curPlayState = animPlay;
            tAnimator.SetBool("SHOOT", animPlay);

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!closeTargets.Contains(other.gameObject) && other.tag == "enemy")
        {
            closeTargets.Add(other.gameObject);
            Debug.Log("TARGET ADDED.");
            if (tTarget == null)
            {
                ChooseTarget();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (closeTargets.Contains(other.gameObject))
        {
            closeTargets.Remove(other.gameObject);
            Debug.Log("TARGET OUT-OF-RANGE.");
        }
    }

    void ChooseTarget()
    {
        int closest_i = 0;
        float closest_dist = range + 100f;
        for (int i=0; i<closeTargets.Count; i++)
        {
            float dist = Vector3.Distance(gameObject.transform.position, closeTargets[i].GetComponent<Transform>().position);
            if (dist < closest_dist)
            {
                closest_dist = dist;
                closest_i = i;
            }
        }

        tTarget = closeTargets[closest_i].GetComponent<Transform>();
        Debug.Log("NEW TARGET (time:" + Time.time + ")");
    }

    void RefreshTargetList()
    {
        closeTargets.RemoveAll(GameObject => GameObject == null);
    }
}
