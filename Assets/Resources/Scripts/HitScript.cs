﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScript : MonoBehaviour
{
    GameObject gSparks;
    Material default_mat;
    Material hit_mat;
    Renderer rd;

    bool hitActive;
    bool hitRecovering;
    float hitTimer;
    public float hitPeriod;

    // Start is called before the first frame update
    void Start()
    {
        gSparks = Resources.Load<GameObject>("Sparks/Sparks");
        hit_mat = Resources.Load<Material>("General/hit_MAT");
        rd = gameObject.GetComponent<Renderer>();

        default_mat = rd.material;

        hitActive = false;
        hitRecovering = false;
        hitTimer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (hitRecovering)
        {
            hitTimer -= Time.deltaTime;

            if (hitTimer <= 0f)
            {
                hitRecovering = false;
            }
        }

        if (hitActive)
        {
            hitTimer -= Time.deltaTime;

            if (hitTimer <= 0f)
            {
                rd.material = default_mat;
                hitRecovering = true;
                hitActive = false;
                hitTimer = hitPeriod;
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (gameObject.name == "Bomber_mesh")
        {
            Debug.Log(collision.collider.name + "  " + collision.collider.tag);
        }

        if (collision.collider.tag == "playerAmmo")
        {
            Vector3 hitDirectionInv = (collision.collider.transform.position - gameObject.transform.position).normalized;
            Vector3 effectLookAt = collision.GetContact(0).point + hitDirectionInv;
            GameObject hitEffect = Instantiate(gSparks, collision.GetContact(0).point, Quaternion.Euler(hitDirectionInv));
            hitEffect.SendMessage("InitHitEffect", effectLookAt);

            if (!hitRecovering && !hitActive)
            {
                hitTimer = hitPeriod;
                hitActive = true;
                rd.material = hit_mat;
            }

            Debug.Log("HIT  " + Time.time);
        }
    }
}
