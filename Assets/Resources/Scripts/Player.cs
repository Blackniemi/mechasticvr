﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    AudioSource audio_minigun;
    AudioClip sfx_minigun_spin;
    AudioClip sfx_minigun_fire;
    AudioClip sfx_minigun_halt;

    GameObject gMinigun;
    Transform tMinigun;

    GameObject pMiniGunAmmo;

    Transform tMinigunAmmoSpawn;
    public float spawnPosNoise;

    public float spinAccelRate;
    public float spinBrakingRate;
    public float spinRate;
    public float maxSpinRate;
    public float spinFireRate;

    public float fireRate;
    public float fireRateTimer;
    
    public enum TriggerState
    {
        UNPRESSED,
        PRESS,
        HOLDING
    };

    public TriggerState triggerState;

    // Start is called before the first frame update
    void Start()
    {
        gMinigun = GameObject.Find("Minigun");
        tMinigun = gMinigun.GetComponent<Transform>();

        pMiniGunAmmo = Resources.Load<GameObject>("Prefabs/MinigunAmmo");

        tMinigunAmmoSpawn = tMinigun.Find("MinigunAmmoSpawn").GetComponent<Transform>();

        audio_minigun = gMinigun.GetComponent<AudioSource>();

        sfx_minigun_spin = Resources.Load<AudioClip>("Audio/minigun_spin");
        sfx_minigun_fire = Resources.Load<AudioClip>("Audio/minigun_fire");
        sfx_minigun_halt = Resources.Load<AudioClip>("Audio/minigun_halt");

        ResetPlayer();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            audio_minigun.Stop();
            audio_minigun.PlayOneShot(sfx_minigun_spin);
        }
        
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            fireRateTimer += Time.deltaTime;

            if (spinRate < maxSpinRate)
            {
                spinRate += spinAccelRate * Time.deltaTime;
                spinRate = Mathf.Clamp(spinRate, 0f, maxSpinRate);
            }

            if (spinRate >= spinFireRate)
            {
                if (!audio_minigun.isPlaying)
                {
                    audio_minigun.PlayOneShot(sfx_minigun_fire);
                }

                if (fireRateTimer >= fireRate)
                {
                    /* FIRE */
                    Vector3 noiseVector = new Vector3(Random.Range(-spawnPosNoise, spawnPosNoise),
                        Random.Range(-spawnPosNoise, spawnPosNoise),
                        Random.Range(-spawnPosNoise, spawnPosNoise));
                    Vector3 spawnPos = tMinigunAmmoSpawn.position + noiseVector;
                    Instantiate(pMiniGunAmmo, spawnPos, tMinigun.rotation);
                    fireRateTimer = 0f;
                }
            }

        }
        else
        {
            if (spinRate > 0f)
            {
                spinRate -= spinBrakingRate * Time.deltaTime;
                spinRate = Mathf.Clamp(spinRate, 0f, maxSpinRate);
            }

            fireRateTimer = 0f;
        }

        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
        {
            audio_minigun.Stop();
            audio_minigun.PlayOneShot(sfx_minigun_halt);
        }

        if (spinRate > 0f)
        {
            tMinigun.Rotate(Vector3.forward, spinRate);
        }
    }

    void ResetPlayer()
    {
        triggerState = TriggerState.UNPRESSED;
        fireRateTimer = 0f;
    }
}
