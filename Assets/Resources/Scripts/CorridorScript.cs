﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorScript : MonoBehaviour
{
    GameObject pBlulit;

    public Vector3 litSpawnPos;
    public float litSpawnTimer;
    public float litSpawnInterval;


    // Start is called before the first frame update
    void Start()
    {

        pBlulit = Resources.Load<GameObject>("Prefabs/Blulit");
        litSpawnPos = GameObject.Find("LitSpawn").GetComponent<Transform>().position;

        litSpawnTimer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        litSpawnTimer += Time.deltaTime;

        if (litSpawnTimer >= litSpawnInterval)
        {
            Instantiate(pBlulit, litSpawnPos, Quaternion.identity);
            litSpawnTimer = 0f;
        }
    }
}
