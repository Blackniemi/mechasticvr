﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitScript : MonoBehaviour
{

    public float litSpeed;
    public float litTravelDist;
    public float litTravelCounter;

    // Start is called before the first frame update
    void Start()
    {
        litTravelCounter = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0f, 0f, -litSpeed));
        litTravelCounter += litSpeed;
        
        if (litTravelCounter > litTravelDist)
        {
            Destroy(gameObject);
        }
    }
}
