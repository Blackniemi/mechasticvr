﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechaMovement2 : MonoBehaviour
{
    AudioSource upperAudio;
    AudioSource lowerAudio;
    AudioClip sfx_upper_turn;
    AudioClip sfx_rocket_thruster;

    public bool canMoveUpper;
    public bool canMoveLower;
    public bool canUseRocket;

    GameObject gUpper;
    GameObject gLower;

    Transform tUpper;
    Transform tLower;
    public Transform tCockpit;

    public float upperTurnSpeed;
    public float upperNodSpeed;
    public float upperInclinationMax;
    public float upperDeclinationMax;

    public float lowerMoveSpeed;
    public float lowerStrafeSpeed;

    Rigidbody rb;
    public float rocketPower;
    public float rocketEnergy;
    public float rocketEnergyMax;
    public bool groundTouched;

    // Start is called before the first frame update
    void Start()
    {
        sfx_upper_turn = Resources.Load<AudioClip>("Audio/Mecha/mecha_upper_turn");
        sfx_rocket_thruster = Resources.Load<AudioClip>("Audio/rocket_thruster_loop");

        gUpper = GameObject.Find("Mech0.2V");
        gLower = GameObject.Find("Mech0.2V");
        tUpper = gameObject.GetComponent<Transform>();
        tLower = gameObject.GetComponent<Transform>();
        tCockpit = gameObject.transform.Find("Cockpit");

        upperAudio = gUpper.GetComponent<AudioSource>();
        lowerAudio = gLower.GetComponent<AudioSource>();

        rb = GameObject.Find("Mech0.2V").GetComponent<Rigidbody>();

        canMoveUpper = true;
        canMoveLower = true;
        canUseRocket = true;
        groundTouched = true;
        rocketEnergy = rocketEnergyMax;

    }

    // Update is called once per frame
    void Update()
    {

        if (canMoveUpper)
        {
            /* Apply rotation to mecha upper body */
            float lookStickHorizontal = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x;
            float lookStickVertical = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).y;

            if (lookStickHorizontal != 0f)
            {
                tLower.Rotate(Vector3.up, upperTurnSpeed * lookStickHorizontal * Time.deltaTime, Space.World);
            }

            if (lookStickVertical != 0f)
            {
                Vector3 rot = tCockpit.rotation.eulerAngles;
                float inclination = upperNodSpeed * -lookStickVertical * Time.deltaTime;
                float newInclination = rot.x + inclination;

                if (newInclination < 90) /* Nodding */
                {
                    if (newInclination > upperDeclinationMax)
                    {
                        newInclination = upperDeclinationMax;
                    }
                }
                else /* Raising head */
                {
                    if (newInclination < 360 - upperInclinationMax)
                    {
                        newInclination = 360 - upperInclinationMax;
                    }
                }

                tCockpit.rotation = Quaternion.Euler(newInclination, rot.y, rot.z);
            }
        }

        if (canMoveLower)
        {
            Vector2 moveStick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
            Vector3 moveDirection = new Vector3(-moveStick.y, 0f, moveStick.x);

            tLower.Translate(moveDirection * lowerMoveSpeed * Time.deltaTime, Space.Self);
        }

        if (canUseRocket)
        {
            bool pressingTrigger = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger);
            bool releasedTrigger = OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger);

            /* If have touched ground and button has been let go, then reset rocket */
            if (!pressingTrigger && groundTouched)
            {
                rocketEnergy = rocketEnergyMax;
            }

            /* If rocket is ready, button press is detected, and there is still energy, use rocket */
            if (pressingTrigger && rocketEnergy > 0f)
            {
                rb.AddForce(Vector3.up * rocketPower, ForceMode.Impulse);
                rocketEnergy -= Time.deltaTime;
                groundTouched = false;

                if (lowerAudio.clip != sfx_rocket_thruster)
                {
                    lowerAudio.loop = true;
                    lowerAudio.clip = sfx_rocket_thruster;
                    lowerAudio.Play();
                }
            }

            if (releasedTrigger || rocketEnergy <= 0f)
            {
                if (lowerAudio.isPlaying && lowerAudio.clip == sfx_rocket_thruster)
                {
                    lowerAudio.loop = false;
                    lowerAudio.Stop();
                    lowerAudio.clip = null;
                }
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.CompareTag("ground"))
        {
            groundTouched = true;
        }
    }
}
