﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareScript : MonoBehaviour
{
    Vector3 flareDestination;
    public float flareSpeed;
    public bool landed;
    public float lifeTimeAfterLanding;

    // Start is called before the first frame update
    void Start()
    {
        landed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!landed)
        {
            if (Vector3.Distance(gameObject.transform.position, flareDestination) <= 0.3f)
            {
                landed = true;
                ParticleSystem.EmissionModule pe = gameObject.GetComponent<ParticleSystem>().emission;
                pe.enabled = false;
                    
                Debug.Log("FLARE SET");
            }
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, flareDestination, flareSpeed * Time.deltaTime);
        }
        else
        {
            lifeTimeAfterLanding -= Time.deltaTime;
            if (lifeTimeAfterLanding <= 0f)
            {
                Destroy(gameObject);
            }
        }

        gameObject.transform.rotation = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
    }

    public void SetFlarePoint(Vector3 dropPoint)
    {
        flareDestination = dropPoint;
    }
}
