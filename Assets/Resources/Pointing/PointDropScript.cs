﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDropScript : MonoBehaviour
{
    GameObject dropAreaCirclePrefab;
    GameObject dropAreaCircle;
    GameObject dropFlarePrefab;
    Transform tFlareGun;

    public GameObject gLeftIndexTip;
    public Transform tLeftIndexTip;

    public int dropAreaLayer;
    int dropAreaLayerMask;

    Gradient pointGradient;
    Gradient targetGradient;

    LineRenderer LR;

    GameObject airDropPrefab;

    public float dropSpawnHeight;
    public float dropSpawnRadiusVariation;

    // Start is called before the first frame update
    void Start()
    {
        dropAreaCirclePrefab = Resources.Load<GameObject>("Pointing/DropAreaCircle");
        dropAreaCircle = null;

        dropFlarePrefab = Resources.Load<GameObject>("Flare/Flare");
        tFlareGun = GameObject.Find("FlareGun").GetComponent<Transform>();

        airDropPrefab = Resources.Load<GameObject>("AirDrop/AirDrop");

        LR = gameObject.GetComponent<LineRenderer>();
        dropAreaLayerMask = 1 << dropAreaLayer;

        LR.material = new Material(Shader.Find("Sprites/Default"));
        LR.widthMultiplier = 0.02f;

        // A simple 2 color gradient with a fixed alpha of 1.0f.
        float alpha = 0.3f;

        pointGradient = new Gradient();
        pointGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(Color.yellow, 0.0f), new GradientColorKey(Color.yellow, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );

        targetGradient = new Gradient();
        targetGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(Color.cyan, 0.0f), new GradientColorKey(Color.cyan, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );

    }

    // Update is called once per frame
    void Update()
    {
        bool dropFirePressed = OVRInput.GetDown(OVRInput.Button.One);

        if (OVRInput.Get(OVRInput.Button.SecondaryHandTrigger) && !OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger))
        {
            if (null == gLeftIndexTip || null == tLeftIndexTip)
            {
                gLeftIndexTip = GameObject.Find("hands:b_r_index_ignore");
                tLeftIndexTip = gLeftIndexTip.GetComponent<Transform>();
            }

            /* Pointing with right hand */
            RaycastHit hit;

            LR.positionCount = 2;
            Vector3 p1 = tLeftIndexTip.position;
            Vector3 p2 = p1 + tLeftIndexTip.TransformDirection(Vector3.right) * 200;
            Vector3[] linePoints = { p1, p2 };
            LR.SetPositions(linePoints);


            if (Physics.Raycast(tLeftIndexTip.position, tLeftIndexTip.TransformDirection(Vector3.right), out hit, Mathf.Infinity, dropAreaLayerMask))
            {
                LR.colorGradient = targetGradient;

                if (null == dropAreaCircle)
                {
                    dropAreaCircle = Instantiate(dropAreaCirclePrefab, hit.point, Quaternion.Euler(Vector3.up)) as GameObject;
                }
                else
                {
                    dropAreaCircle.GetComponent<Transform>().position = hit.point;
                }
                //Debug.Log("Dropzone in sight!");

                if (dropFirePressed)
                {
                    SpawnDrop(hit.point);
                }
            }
            else
            {
                LR.colorGradient = pointGradient;
                if (null != dropAreaCircle)
                {
                    Destroy(dropAreaCircle);
                    dropAreaCircle = null;
                }
            }
        }
        else
        {
            LR.positionCount = 0;
            if (null != dropAreaCircle)
            {
                Destroy(dropAreaCircle);
                dropAreaCircle = null;
            }
        }
    }

    void SpawnDrop(Vector3 dropPoint)
    {
        Vector3 spawnRadiusOffset = new Vector3(Mathf.Sin(Random.Range(0f, 100)), 0f, Mathf.Sin(Random.Range(0f, 50)));

        Vector3 spawnPos = dropPoint + (Vector3.up * dropSpawnHeight) + (spawnRadiusOffset * dropSpawnRadiusVariation);

        GameObject drop = Instantiate(airDropPrefab, spawnPos, Quaternion.identity) as GameObject;
        drop.GetComponent<AirDropAnimationController>().ConfigDrop(dropPoint, AirDropAnimationController.Turrets.RAILGUN);

        GameObject flare = Instantiate(dropFlarePrefab, tFlareGun.position, Quaternion.identity) as GameObject;
        flare.GetComponent<FlareScript>().SetFlarePoint(dropPoint);
    }
}
