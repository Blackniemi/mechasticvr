﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeakerScript : MonoBehaviour
{
    Transform tMiddle;
    public float talkWobble;
    public float shoutWobble;
    public float wobble;

    public enum SpeakerMood
    {
        SILENT,
        TALKING,
        SHOUTING
    };

    public SpeakerMood speakerMood;

    // Start is called before the first frame update
    void Start()
    {
        tMiddle = gameObject.transform.Find("SpeakerMiddle");
        speakerMood = SpeakerMood.SILENT;
    }

    // Update is called once per frame
    void Update()
    {
        tMiddle.localPosition = new Vector3(0f, 0f, Random.Range(-wobble, 0f));
    }

    public void SetMood(SpeakerMood mood)
    {
        switch (mood)
        {
            case SpeakerMood.SILENT:
                wobble = 0f;
                break;

            case SpeakerMood.TALKING:
                wobble = talkWobble;
                break;

            case SpeakerMood.SHOUTING:
                wobble = shoutWobble;
                break;
        }
        speakerMood = mood;
    }
}
