﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBeltScript : MonoBehaviour
{
    public float orbitRadius;
    public float orbitSpeed;
    public float asteroidGroupingAngle;
    public float asteroidMaxSpread;
    public float asteroidCount;

    GameObject gAsteroid1;
    GameObject gAsteroid2;
    GameObject gAsteroid3;
    GameObject gAsteroid4;
    GameObject gAsteroid5;

    List<GameObject> gAsteroidPrefabs;
    List<Transform> tAsteroids;

    // Start is called before the first frame update
    void Start()
    {
        gAsteroid1 = Resources.Load<GameObject>("Environment/Asteroids/Asteroid1");
        gAsteroid2 = Resources.Load<GameObject>("Environment/Asteroids/Asteroid2");
        gAsteroid3 = Resources.Load<GameObject>("Environment/Asteroids/Asteroid3");
        gAsteroid4 = Resources.Load<GameObject>("Environment/Asteroids/Asteroid4");
        gAsteroid5 = Resources.Load<GameObject>("Environment/Asteroids/Asteroid5");

        tAsteroids = new List<Transform>();
        gAsteroidPrefabs = new List<GameObject>();
        gAsteroidPrefabs.Add(gAsteroid1);
        gAsteroidPrefabs.Add(gAsteroid2);
        gAsteroidPrefabs.Add(gAsteroid3);
        gAsteroidPrefabs.Add(gAsteroid4);
        gAsteroidPrefabs.Add(gAsteroid5);

        gameObject.transform.localRotation = Quaternion.Euler(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));

        Vector3 orbitSpawnPos = gameObject.transform.up * orbitRadius;

        for (int i=0; i<asteroidCount; i++)
        {
            Vector3 orbitSpawnSpreadPos = Quaternion.AngleAxis(Random.Range(-2f - asteroidMaxSpread, asteroidMaxSpread + 2f), gameObject.transform.forward) * orbitSpawnPos;
            GameObject rock = Instantiate(gAsteroidPrefabs[Random.Range(0, gAsteroidPrefabs.Count - 1)], orbitSpawnSpreadPos, Quaternion.identity, gameObject.transform);
            tAsteroids.Add(rock.GetComponent<Transform>());

            orbitSpawnPos = Quaternion.AngleAxis(asteroidGroupingAngle, gameObject.transform.right) * orbitSpawnPos;
        }
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(Vector3.right, orbitSpeed * Time.deltaTime);
    }
}
