﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    public float sizeVariation;
    public float spinVariation;
    public float initialSpinRate;
    float spinRate;
    Vector3 spinAxis;

    // Start is called before the first frame update
    void Start()
    {
        spinRate = initialSpinRate + Random.Range(-spinVariation, spinVariation);
        gameObject.transform.localScale = Vector3.one * 5f + Vector3.one * Random.Range(-sizeVariation, sizeVariation);
        spinAxis = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(spinAxis, spinRate * Time.deltaTime);
    }
}
