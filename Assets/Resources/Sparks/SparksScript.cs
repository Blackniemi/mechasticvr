﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparksScript : MonoBehaviour
{

    public float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0f)
        {
            Destroy(gameObject);
        }
    }

    public void InitHitEffect(Vector3 effectLookAt)
    {
        gameObject.transform.LookAt(effectLookAt);

        gameObject.GetComponent<ParticleSystem>().Emit(5);
    }
}
