﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailGunAmmoScript : MonoBehaviour
{
    public float ammoSpeed;
    public float ammoLifeTime;

    // Start is called before the first frame update
    void Start()
    {
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * ammoSpeed * Time.deltaTime;

        ammoLifeTime -= Time.deltaTime;

        if (ammoLifeTime <= 0f)
            Destroy(gameObject);
    }

    public void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
